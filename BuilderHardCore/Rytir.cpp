//
// Created by gregor on 07/01/2020.
//

#include "Rytir.h"

rytiri::Rytir::Rytir(string jmeno, int sila) {
    m_jmeno = jmeno;
    m_sila = sila;
    m_helma = 0;
    m_zbroj = 0;
}

int rytiri::Rytir::getUtok() {
    int utok = m_sila;
    if (m_zbroj != 0) {
        utok+=m_zbroj->getBonusUtoku();
    }
    return utok;
}

int rytiri::Rytir::getObrana() {
    int obrana = m_sila;
    if (m_zbroj != 0) {
        obrana+=m_zbroj->getBonusObrany();
    }
    if (m_helma != 0) {
        obrana+=m_helma->getBonusObrany();
    }
    return obrana;
}

void rytiri::Rytir::setZbroj(Brneni* brneni) {
    m_zbroj = brneni;
}

void rytiri::Rytir::setHelma(Helma* helma) {
    m_helma = helma;
}

void rytiri::Rytir::print() {
    cout << "------Rytir-------" << endl;
    cout << "jmeno :" << m_jmeno << endl;
    cout << "sila :" << m_sila << endl;
    if (m_helma != 0) {
        m_helma->printInfo();
    }
    if (m_helma != 0) {
        m_zbroj->printInfo();
    }
    cout << "------------------" << endl;
}