//
// Created by gregor on 07/01/2020.
//

#ifndef RYTIRDIRECTOR_H
#define RYTIRDIRECTOR_H
#include <iostream>
#include "RytirBuilder.h"

using namespace std;

namespace rytiri {
    class RytirDirector {

    private:
        rytiri::RytirBuilder* m_rytirBuilder;

    public:
        RytirDirector(rytiri::RytirBuilder* rytirBuilder);

        void setRytirBuilder(rytiri::RytirBuilder* rytirBuilder);

        rytiri::Rytir* createRytir(string jmeno, int sila, int vahaBrneni, int odolnostBrneni, int ohebnostBrneni, int velikostPlatu, int velikostHelmy);
    };
}
#endif // RYTIRDIRECTOR_H

