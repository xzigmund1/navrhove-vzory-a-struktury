//
// Created by gregor on 07/01/2020.
//

#include "Helma.h"

rytiri::Helma::Helma(int velikost) {
    m_velikost = velikost;
}

void rytiri::Helma::printInfo() {
    cout << "helma:" << endl;
    cout << "-velikost helmy: " << m_velikost << endl;
    cout << "-bonus obrany: " << getBonusObrany() << endl;
}
