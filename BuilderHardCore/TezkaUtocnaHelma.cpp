//
// Created by gregor on 07/01/2020.
//

#include "TezkaUtocnaHelma.h"

rytiri::TezkaUtocnaHelma::TezkaUtocnaHelma(int velikost):Helma(velikost) {

}

int rytiri::TezkaUtocnaHelma::getBonusObrany() {
    return m_velikost;
}

void rytiri::TezkaUtocnaHelma::printInfo() {
    Helma::printInfo();
    cout << "-typ: tezka obrana helma" << endl;
}
