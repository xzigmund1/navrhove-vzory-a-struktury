//
// Created by gregor on 07/01/2020.
//

#ifndef BRENENI_H
#define BRENENI_H
#include <iostream>

using namespace std;

namespace rytiri {
    class Brneni {

    private:
        int m_vaha;
    protected:
        int m_odolnost;

    public:
        Brneni(int vaha, int odolnost);

        virtual int getBonusUtoku() = 0;

        virtual int getBonusObrany() = 0;

        void printInfo();
    };
}
#endif // BRENENI_H
