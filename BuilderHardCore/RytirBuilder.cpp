//
// Created by gregor on 07/01/2020.
//

#include "RytirBuilder.h"

void rytiri::RytirBuilder::createRytir(string jmeno, int sila) {
    m_rytir = new Rytir(jmeno, sila);
}

rytiri::Rytir* rytiri::RytirBuilder::getRytir() {
    return m_rytir;
}
