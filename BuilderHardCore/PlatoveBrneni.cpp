//
// Created by gregor on 07/01/2020.
//

#include "PlatoveBrneni.h"

rytiri::PlatoveBrneni::PlatoveBrneni(int vaha, int odolnost, int velikostPlatu):Brneni(vaha,odolnost) {
    m_velikostplatu = velikostPlatu;
}

int rytiri::PlatoveBrneni::getBonusUtoku() {
    return m_odolnost/2;
}

int rytiri::PlatoveBrneni::getBonusObrany() {
    return m_odolnost;
}

void rytiri::PlatoveBrneni::printInfo() {
    Brneni::printInfo();
    cout << "-typ: platove brneni" << endl;
    cout << "-velikostPlatu: " << m_velikostplatu << endl;
}
