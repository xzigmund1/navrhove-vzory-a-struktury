//
// Created by gregor on 07/01/2020.
//

#ifndef RYTIRBUILDER_H
#define RYTIRBUILDER_H
#include <iostream>
#include "Rytir.h"

using namespace std;
namespace rytiri {
    class RytirBuilder {

    protected:
        rytiri::Rytir* m_rytir;

    public:
        void createRytir(string jmeno, int sila);

        rytiri::Rytir* getRytir();

        virtual void buildHelma(int velikost) = 0;

        virtual void buildBrneni(int vaha, int odolnost, int ohebnost, int velikostPlatu) = 0;
    };
}
#endif // RYTIRBUILDER_H

