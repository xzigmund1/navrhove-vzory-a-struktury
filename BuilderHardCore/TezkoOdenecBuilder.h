//
// Created by gregor on 07/01/2020.
//

#ifndef TEZKOODENECBUILDER_H
#define TEZKOODENECBUILDER_H
#include <iostream>
#include "RytirBuilder.h"
#include "TezkaUtocnaHelma.h"
#include "PlatoveBrneni.h"

using namespace std;

namespace rytiri {
    class TezkoOdenecBuilder : public rytiri::RytirBuilder {


    public:
        void buildHelma(int velikost);

        void buildBrneni(int vaha, int odolnost, int ohebnost, int velikostPlatu);
    };
}

#endif // TEZKOODENECBUILDER_H
