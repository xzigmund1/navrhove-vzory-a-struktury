//
// Created by gregor on 07/01/2020.
//

#ifndef HELMA_H
#define HELMA_H
#include <iostream>

using namespace std;
namespace rytiri {
    class Helma {

    protected:
        int m_velikost;

    public:
        Helma(int velikost);

        virtual int getBonusObrany() = 0;

        void printInfo();
    };
}
#endif // HELMA_H

