//
// Created by gregor on 07/01/2020.
//

#ifndef KROUZKOVEBRNENI_H
#define KROUZKOVEBRNENI_H
#include <iostream>
#include "Brneni.h"

using namespace std;
namespace rytiri {
    class KrouzkoveBrneni : public rytiri::Brneni {

    public:
        int m_ohebnost;

        KrouzkoveBrneni(int vaha, int odolnost, int ohebnost);

        int getBonusObrany();

        int getBonusUtoku();

        void printInfo();
    };
}
#endif // KROUZKOVEBRNENI_H

