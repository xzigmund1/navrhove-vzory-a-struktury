//
// Created by gregor on 07/01/2020.
//

#include "LehkoOdenecBuilder.h"

void rytiri::LehkoOdenecBuilder::buildHelma(int velikost) {
    m_rytir->setHelma(new LehkaUtocnaHelma(velikost));
}

void rytiri::LehkoOdenecBuilder::buildBrneni(int vaha, int odolnost, int ohebnost, int velikostPlatu) {
    m_rytir->setZbroj(new KrouzkoveBrneni(vaha, odolnost, ohebnost));
}
