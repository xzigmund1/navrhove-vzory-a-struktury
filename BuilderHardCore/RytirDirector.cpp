//
// Created by gregor on 07/01/2020.
//

#include "RytirDirector.h"

rytiri::RytirDirector::RytirDirector(rytiri::RytirBuilder* rytirBuilder) {
    m_rytirBuilder = rytirBuilder;
}

void rytiri::RytirDirector::setRytirBuilder(rytiri::RytirBuilder* rytirBuilder) {
    m_rytirBuilder = rytirBuilder;
}

rytiri::Rytir* rytiri::RytirDirector::createRytir(string jmeno, int sila, int vahaBrneni, int odolnostBrneni, int ohebnostBrneni, int velikostPlatu, int velikostHelmy) {
    m_rytirBuilder->createRytir(jmeno, sila);
    m_rytirBuilder->buildHelma(velikostHelmy);
    m_rytirBuilder->buildBrneni(vahaBrneni, odolnostBrneni, ohebnostBrneni, velikostPlatu);

    return m_rytirBuilder->getRytir();
}
