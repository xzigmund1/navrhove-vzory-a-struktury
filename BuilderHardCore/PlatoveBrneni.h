//
// Created by gregor on 07/01/2020.
//

#ifndef PLATOVEBRNENI_H
#define PLATOVEBRNENI_H
#include <iostream>
#include "Brneni.h"

using namespace std;
namespace rytiri {
    class PlatoveBrneni : public rytiri::Brneni {

    public:
        int m_velikostplatu;

        PlatoveBrneni(int vaha, int odolnost, int velikostPlatu);

        int getBonusUtoku();

        int getBonusObrany();

        void printInfo();
    };
}

#endif // PLATOVEBRNENI_H
