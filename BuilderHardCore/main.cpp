#include <iostream>
#include "TezkoOdenecBuilder.h"
#include "LehkoOdenecBuilder.h"
#include "RytirBuilder.h"
#include "RytirDirector.h"

using namespace std;
using namespace rytiri;

int main()
{
    RytirBuilder* stavitelLehkych = new LehkoOdenecBuilder();
    RytirBuilder* stavitelTezkych = new TezkoOdenecBuilder();
    RytirDirector* director = new RytirDirector(stavitelLehkych);

    Rytir* artus = director->createRytir("Artus",50, 5, 3, 8, 1, 52);
    artus->print();

    director->setRytirBuilder(stavitelTezkych);
    Rytir* barbarConan = director->createRytir("Barbar Conan", 100, 10, 10, 10, 10, 100);
    barbarConan->print();

    delete artus;
    delete barbarConan;
    delete director;
    delete stavitelLehkych;
    delete stavitelTezkych;

    return 0;
}
