//
// Created by gregor on 07/01/2020.
//

#include "LehkaUtocnaHelma.h"

rytiri::LehkaUtocnaHelma::LehkaUtocnaHelma(int velikost):Helma(velikost) {
    //nemam zadne dalsi parametry ktere bych nastavoval
}

int rytiri::LehkaUtocnaHelma::getBonusObrany() {
    return 0;
}

void rytiri::LehkaUtocnaHelma::printInfo() {
    Helma::printInfo();
    cout << "-typ: lehka utocna helma" << endl;
}
