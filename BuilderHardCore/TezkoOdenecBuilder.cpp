//
// Created by gregor on 07/01/2020.
//

#include "TezkoOdenecBuilder.h"

void rytiri::TezkoOdenecBuilder::buildHelma(int velikost) {
    m_rytir->setHelma(new TezkaUtocnaHelma(velikost));
}

void rytiri::TezkoOdenecBuilder::buildBrneni(int vaha, int odolnost, int ohebnost, int velikostPlatu) {
    m_rytir->setZbroj(new PlatoveBrneni(vaha, odolnost, ohebnost));
}
