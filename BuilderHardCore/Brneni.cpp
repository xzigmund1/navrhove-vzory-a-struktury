//
// Created by gregor on 07/01/2020.
//

#include "Brneni.h"

rytiri::Brneni::Brneni(int vaha, int odolnost) {
    m_vaha = vaha;
    m_odolnost = odolnost;
}

void rytiri::Brneni::printInfo() {
    cout << "brneni: " << endl;
    cout << "-vaha: " << m_vaha << endl;
    cout << "-odolnost: " << m_odolnost << endl;
    cout << "-bonus utoku: " << getBonusUtoku() << endl;
    cout << "-bonus obrany: " << getBonusObrany() << endl;
}
