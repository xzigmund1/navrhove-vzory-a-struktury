//
// Created by gregor on 07/01/2020.
//

#include "KrouzkoveBrneni.h"

rytiri::KrouzkoveBrneni::KrouzkoveBrneni(int vaha, int odolnost, int ohebnost):Brneni(vaha, odolnost) {
    m_ohebnost = ohebnost;
}

int rytiri::KrouzkoveBrneni::getBonusObrany() {
    return m_odolnost;
}

int rytiri::KrouzkoveBrneni::getBonusUtoku() {
    return m_odolnost/2 + m_ohebnost;
}

void rytiri::KrouzkoveBrneni::printInfo() {
    Brneni::printInfo();
    cout << "-typ: krouzkove brneni" << endl;
    cout << "-odolnost: " << m_odolnost << endl;
    cout << "-ohebnost: " << m_ohebnost << endl;
}
