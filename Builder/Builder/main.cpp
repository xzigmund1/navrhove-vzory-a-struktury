//
//  main.cpp
//  Builder
//
//  Created by David Prochazka on 01.12.13.
//  Copyright (c) 2013 David Prochazka. All rights reserved.
//

#include <iostream>
#include "DocumentDirector.h"
#include "LatexDocumentBuilder.h"
#include "XmlDocumentBuilder.h"

int main(int argc, const char * argv[])
{
	DocumentDirector* director
		= new DocumentDirector(new LatexDocumentBuilder("utf8"));
	Document* helloPage = director->constructDocument("Hello World");

	cout << helloPage->getContent() << endl;
	
	delete director;
	delete helloPage;
    return 0;
}

